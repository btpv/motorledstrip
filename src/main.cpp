
#include <Arduino.h>
#include <FastLED.h>
#include <stdint.h>
#define LEFT 25
#define RIGHT 26
#define BRAKE 32
#define EMERGENCYBRAKE 0
/*------------------------------------------------------------------------------
 * Initialize setup parameters
 *----------------------------------------------------------------------------*/
struct blinker
{
  unsigned long ptime;
  unsigned long lptime;
};
blinker left = { 0, 1 };
blinker right = { 0, 1 };
static const uint8_t LED_PIN = 16;
static const uint8_t length = 48;
static const float segments = 0.5;
static const int segmentsize = length / segments;
static CRGB leds[length];
static const int halflength = (ceil(length / (float)2));
void IRAM_ATTR callbackleft() {
	left.ptime = left.lptime;
	left.lptime = millis();
}
void IRAM_ATTR callbackright() {
	right.ptime = right.lptime;
	right.lptime = millis();
}
void setup()
{
  Serial.begin(115200);
  Serial.println("Starting");
  FastLED.addLeds<WS2812B, LED_PIN, GRB>(leds, sizeof(leds) / sizeof(CRGB));
  FastLED.setBrightness(50);
  FastLED.setDither(false);
  pinMode(EMERGENCYBRAKE, INPUT);
  pinMode(LEFT, INPUT_PULLDOWN);
  pinMode(RIGHT, INPUT_PULLDOWN);
  pinMode(BRAKE, INPUT_PULLDOWN);
  pinMode(LED_BUILTIN, OUTPUT);
  attachInterrupt(LEFT, callbackleft, CHANGE);
	attachInterrupt(RIGHT, callbackright, CHANGE);
    for (int i = 0; i < length; i++){
    leds[i] = CRGB(0, 0, 0);
  }
  FastLED.show();
  for (int i = length/2; i < length; i++){
    leds[i] = CRGB(255, 0, 0);
    leds[length-i] = CRGB(255, 0, 0);
    delay(50);
    FastLED.show();
  }
  for (int i = length; i > length/2; i--){
    leds[i] = CRGB(0, 0, 0);
    leds[length-i] = CRGB(0, 0, 0);
    delay(50);
    FastLED.show();
  }
  for (int i = length/2; i < length; i++){
    leds[i] = CRGB(255, 0, 0);
    leds[length-i] = CRGB(255, 0, 0);
    delay(50);
    FastLED.show();
  }

}
/*------------------------------------------------------------------------------
 * Start the main loop
 *----------------------------------------------------------------------------*/
void loop()
{
  FastLED.setBrightness(digitalRead(BRAKE) || !digitalRead(EMERGENCYBRAKE) ? 255 : 50);
  digitalWrite(LED_BUILTIN, digitalRead(EMERGENCYBRAKE));
  int step = ((millis() / 50) % segmentsize);
  Serial.print(left.lptime-left.ptime);
  Serial.print("\t");
  Serial.print(millis()-left.lptime);
  Serial.print("\t");
  Serial.print(right.lptime-right.ptime);
  Serial.print("\t");
  Serial.print(millis()-right.lptime);
  Serial.print("\t");
  Serial.println(step);
  bool leftstate = (left.lptime-left.ptime > 300 && millis()-left.lptime < 405) || digitalRead(LEFT);
  bool rightstate = (right.lptime-right.ptime > 300 && millis()-right.lptime < 405) || digitalRead(RIGHT);
  for (int i = 0; i < length; i++)
  {
    if (!digitalRead(EMERGENCYBRAKE))
    {
      leds[i] = ((millis() / 100) % 2) ? CRGB(255, 0, 0) : CRGB(0, 0, 0);
    }
    else if (leftstate && rightstate)
    {
      leds[i] = (digitalRead(LEFT) && digitalRead(RIGHT)) ? CRGB(255, 30, 0) : CRGB(digitalRead(BRAKE) ? 255 : 20, 0, 0);
    }
    else if (leftstate)
    {
      leds[i] = (((i - step + segmentsize) % segmentsize < segmentsize / 2)) ? CRGB(255, 30, 0) : CRGB(digitalRead(BRAKE) ? 127 : 20, 0, 0);
    }
    else if (rightstate)
    {
      leds[i] = (((i + step) % segmentsize < segmentsize / 2)) ? CRGB(255, 30, 0) : CRGB(digitalRead(BRAKE) ? 127 : 20, 0, 0);
    }
    else
    {
      leds[i] = CRGB(255, 0, 0);
    }
  }
  FastLED.show();
}